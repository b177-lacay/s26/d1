// http 
// let node.js transfer data using http(hypertext transfer protocol) + require cmd
let http = require("http");
// pwede ilagay as variable para hindi lagi nagtytype ng require http

// Creating the server
// createServer() method

http.createServer(function (request, response) {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello,World from Batch 177');
	}).listen(4000);
// the network should listen in 4000 port
// create a console.log to indicate that the server is running.

console.log('Server running at localhost:4000')